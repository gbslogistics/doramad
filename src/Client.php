<?php

namespace GbsLogistics\Doramad;


use GbsLogistics\Doramad\Domain\CrestResourceInterface;
use GbsLogistics\Doramad\Model\CrestCollection;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Message\ResponseInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;

class Client extends GuzzleClient
{
    const CREST_CONTENT_TYPE_PATTERN = '#application/vnd\.ccp\.eve\.(\w+?)(Collection)?-v\d\+json#';
    const TARGET_NAMESPACE = 'GbsLogistics\\Doramad\\Domain\\';

    /** @var Serializer */
    private $serializer;

    function __construct(array $config = [], SerializerInterface $serializerInterface = null)
    {
        parent::__construct($config);
        $this->serializer = $serializerInterface ?: SerializerFactory::getSerializer();
    }

    /**
     * @param $url
     * @throws CrestException
     * @return CrestResourceInterface|array<CrestResourceInterface>
     */
    public function loadCrestResource($url)
    {
        /** @noinspection PhpVoidFunctionResultUsedInspection */
        /** @var ResponseInterface $resource */
        $resource = $this->get($url);
        $contentType = $resource->getHeader('Content-Type');
        $matches = [];

        if (!preg_match(self::CREST_CONTENT_TYPE_PATTERN, $contentType, $matches)) {
            throw new CrestException(sprintf(
                'Return type for url "%s" was not a valid CREST content type; got "%s" instead.',
                $url,
                $contentType
            ));
        }

        $contentClass = self::TARGET_NAMESPACE . $matches[1];
        $isCollection = isset($matches[2]) ? $matches[2] === 'Collection' : false;

        if (!class_exists($contentClass)) {
            throw new CrestException(sprintf(
                'Class "%s" does not exist; derived from content type "%s".',
                $contentClass,
                $contentType
            ));
        } elseif (false === $implements = class_implements($contentClass)) {
            throw new CrestException(sprintf(
                'Cannot load invalid class "%s"; derived from content type "%s"',
                $contentClass,
                $contentType
            ));
        } elseif (!isset($implements[CrestResourceInterface::class])) {
            throw new CrestException(sprintf(
                'Class "%s" derived from content type "%s" is not a Crest Resource.',
                $contentClass,
                $contentType
            ));
        }

        $data = (string)$resource->getBody();

        if ($isCollection) {
            $context = new CrestDeserializationContext();
            $context->setCollectionClass($contentClass);

            /** @var CrestCollection $collection */
            $collection = $this->parseObject(CrestCollection::class, $data, $context);

            return $collection->getItems();
        } else {
            return $this->parseObject($contentClass, $data);
        }
    }

    protected function parseObject($contentClass, $data, DeserializationContext $context = null)
    {
        return $this->serializer->deserialize($data, $contentClass, 'json', $context);
    }

    public static function getDefaultUserAgent()
    {
        /** @noinspection SpellCheckingInspection */
        return 'Doramad CREST Client (contact: querns@gbs.io)';
    }
}
