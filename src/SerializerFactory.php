<?php

namespace GbsLogistics\Doramad;


use JMS\Serializer\Handler\ArrayCollectionHandler;
use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Handler\PhpCollectionHandler;
use JMS\Serializer\SerializerBuilder;

class SerializerFactory
{
    /**
     * @return SerializerBuilder
     */
    public static function getBuilder()
    {
        return SerializerBuilder::create()
            ->setMetadataDirs([
                'GbsLogistics\\Doramad\\Domain' => __DIR__ . '/../meta/serializer/domain',
                'GbsLogistics\\Doramad\\Model' => __DIR__ . '/../meta/serializer/model',
            ])
            ->addDefaultHandlers()
            ->configureHandlers(function (HandlerRegistry $registry) {
                $registry->registerSubscribingHandler(new ExtractionHandler());
                $registry->registerSubscribingHandler(new CrestCollectionHandler());
            });
    }

    /**
     * @return \JMS\Serializer\Serializer
     */
    public static function getSerializer()
    {
        return self::getBuilder()->build();
    }
}
