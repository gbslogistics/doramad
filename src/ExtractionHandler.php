<?php

namespace GbsLogistics\Doramad;


use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;

class ExtractionHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'extract',
                'method' => 'deserializeGroup'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'extract',
                'method' => 'serializeGroup'
            ],
        ];
    }

    public function deserializeGroup(
        JsonDeserializationVisitor $visitor,
        array $data,
        array $type,
        Context $context
    ) {
        $key = $type['params'][0];
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }
    }

    public function serializeGroup(
        JsonSerializationVisitor $visitor,
        $data,
        array $type,
        Context $context
    ) {
        $key = $type['params'][0];
        $keyWithStrAppended = sprintf('%s_str', $key);

        return [ $key => $data, $keyWithStrAppended => $data ];
    }
}
