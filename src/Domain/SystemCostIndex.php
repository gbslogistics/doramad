<?php

namespace GbsLogistics\Doramad\Domain;


/**
 * @author Querns <querns@gbs.io>
 */
class SystemCostIndex
{
    /** @var float */
    protected $costIndex;

    /** @var int */
    protected $activityId;

    /** @var string */
    protected $activityName;

    /**
     * @param int $activityId
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;
    }

    /**
     * @return int
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * @param string $activityName
     */
    public function setActivityName($activityName)
    {
        $this->activityName = $activityName;
    }

    /**
     * @return string
     */
    public function getActivityName()
    {
        return $this->activityName;
    }

    /**
     * @param float $costIndex
     */
    public function setCostIndex($costIndex)
    {
        $this->costIndex = $costIndex;
    }

    /**
     * @return float
     */
    public function getCostIndex()
    {
        return $this->costIndex;
    }
}

