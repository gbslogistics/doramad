<?php
namespace GbsLogistics\Doramad\Domain;


class SolarSystem
{
    /** @var int */
    protected $solarSystemId;
    /** @var string */
    protected $name;

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $solarSystemId
     * @return $this
     */
    public function setSolarSystemId($solarSystemId)
    {
        $this->solarSystemId = $solarSystemId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSolarSystemId()
    {
        return $this->solarSystemId;
    }

}
