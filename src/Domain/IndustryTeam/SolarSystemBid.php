<?php


namespace GbsLogistics\Doramad\Domain\IndustryTeam;

use GbsLogistics\Doramad\Domain\SolarSystem;

class SolarSystemBid
{
    /** @var SolarSystem */
    protected $solarSystem;
    /** @var array<CharacterBid> */
    protected $characterBids = [];
    /** @var float */
    protected $bidAmount;

    function __construct()
    {
        $this->solarSystem = new SolarSystem();
    }

    /**
     * @param float $bidAmount
     * @return $this
     */
    public function setBidAmount($bidAmount)
    {
        $this->bidAmount = $bidAmount;
        return $this;
    }

    /**
     * @return float
     */
    public function getBidAmount()
    {
        return $this->bidAmount;
    }

    /**
     * @param array $characterBids
     * @return $this
     */
    public function setCharacterBids($characterBids)
    {
        $this->characterBids = $characterBids;
        return $this;
    }

    /**
     * @return array
     */
    public function getCharacterBids()
    {
        return $this->characterBids;
    }

    /**
     * @param \GbsLogistics\Doramad\Domain\SolarSystem $solarSystem
     * @return $this
     */
    public function setSolarSystem($solarSystem)
    {
        $this->solarSystem = $solarSystem;
        return $this;
    }

    /**
     * @return \GbsLogistics\Doramad\Domain\SolarSystem
     */
    public function getSolarSystem()
    {
        return $this->solarSystem;
    }


}
