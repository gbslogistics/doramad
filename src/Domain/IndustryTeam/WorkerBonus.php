<?php
namespace GbsLogistics\Doramad\Domain\IndustryTeam;

use GbsLogistics\Doramad\Domain\SpecializationTypeEnum;

class WorkerBonus
{
    /** @var int */
    protected $bonusType;
    /** @var int */
    protected $bonusId;
    /** @var float */
    protected $value;

    /**
     * @param int $bonusId
     * @return $this
     */
    public function setBonusId($bonusId)
    {
        $this->bonusId = $bonusId;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusId()
    {
        return $this->bonusId;
    }

    /**
     * @param string $bonusType
     * @return $this
     */
    public function setBonusType($bonusType)
    {
        $this->bonusType = SpecializationTypeEnum::INVALID;
        if ($bonusType === 'ME') {
            $this->bonusType = SpecializationTypeEnum::ME;
        } elseif ($bonusType === 'TE') {
            $this->bonusType = SpecializationTypeEnum::TE;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getBonusType()
    {
        return $this->bonusType;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

}
