<?php

namespace GbsLogistics\Doramad\Domain\IndustryTeam;

use GbsLogistics\Doramad\Domain\Character;

class CharacterBid
{
    /** @var Character */
    protected $character;
    /** @var float */
    protected $bidAmount;

    function __construct()
    {
        $this->character = new Character();
    }

    /**
     * @param float $bidAmount
     * @return $this
     */
    public function setBidAmount($bidAmount)
    {
        $this->bidAmount = $bidAmount;
        return $this;
    }

    /**
     * @return float
     */
    public function getBidAmount()
    {
        return $this->bidAmount;
    }

    /**
     * @param \GbsLogistics\Doramad\Domain\Character $character
     * @return $this
     */
    public function setCharacter($character)
    {
        $this->character = $character;
        return $this;
    }

    /**
     * @return \GbsLogistics\Doramad\Domain\Character
     */
    public function getCharacter()
    {
        return $this->character;
    }


}
