<?php
namespace GbsLogistics\Doramad\Domain\IndustryTeam;


use GbsLogistics\Doramad\Domain\IndustrySpeciality;

class Worker
{
    /** @var WorkerBonus */
    protected $bonus;
    /** @var integer */
    protected $specializationId;

    function __construct()
    {
        $this->bonus = new WorkerBonus();
    }

    /**
     * @param WorkerBonus $bonus
     * @return $this
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
        return $this;
    }

    /**
     * @return WorkerBonus
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param integer $specialization
     * @return $this
     */
    public function setSpecializationId($specialization)
    {
        $this->specializationId = $specialization;
        return $this;
    }

    /**
     * @return integer
     */
    public function getSpecializationId()
    {
        return $this->specializationId;
    }
}
