<?php

namespace GbsLogistics\Doramad\Domain;


class IndustryFacility implements CrestResourceInterface
{
    /** @var int */
    private $facilityId;

    /** @var int */
    private $solarSystemId;

    /** @var string */
    private $facilityName;

    /** @var int */
    private $regionId;

    /** @var float */
    private $tax;

    /** @var int */
    private $ownerId;

    /** @var int */
    private $typeId;

    /**
     * @return int
     */
    public function getFacilityId()
    {
        return $this->facilityId;
    }

    /**
     * @param int $facilityID
     */
    public function setFacilityId($facilityID)
    {
        $this->facilityId = $facilityID;
    }

    /**
     * @return string
     */
    public function getFacilityName()
    {
        return $this->facilityName;
    }

    /**
     * @param string $facilityName
     */
    public function setFacilityName($facilityName)
    {
        $this->facilityName = $facilityName;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param int $ownerID
     */
    public function setOwnerId($ownerID)
    {
        $this->ownerId = $ownerID;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * @param int $regionID
     */
    public function setRegionId($regionID)
    {
        $this->regionId = $regionID;
    }

    /**
     * @return int
     */
    public function getSolarSystemId()
    {
        return $this->solarSystemId;
    }

    /**
     * @param int $solarSystemID
     */
    public function setSolarSystemId($solarSystemID)
    {
        $this->solarSystemId = $solarSystemID;
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int $typeID
     */
    public function setTypeId($typeID)
    {
        $this->typeId = $typeID;
    }


}
