<?php
namespace GbsLogistics\Doramad\Domain;


class Character
{
    /** @var int */
    protected $characterId;
    /** @var string */
    protected $name;
    /** @var boolean */
    protected $isNpc;

    /**
     * @param int $characterId
     * @return $this
     */
    public function setCharacterId($characterId)
    {
        $this->characterId = $characterId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCharacterId()
    {
        return $this->characterId;
    }

    /**
     * @param boolean $isNpc
     * @return $this
     */
    public function setIsNpc($isNpc)
    {
        $this->isNpc = $isNpc;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsNpc()
    {
        return $this->isNpc;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
