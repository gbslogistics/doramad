<?php

namespace GbsLogistics\Doramad\Domain;


/**
 * @author Querns <querns@gbs.io>
 */
class IndustrySystem implements CrestResourceInterface
{
    /** @var string */
    protected $id;

    /** @var array<SystemCostIndex> */
    protected $systemCostIndices = [];

    /** @var SolarSystem */
    protected $solarSystem;

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \GbsLogistics\Doramad\Domain\SolarSystem $solarSystem
     */
    public function setSolarSystem($solarSystem)
    {
        $this->solarSystem = $solarSystem;
    }

    /**
     * @return \GbsLogistics\Doramad\Domain\SolarSystem
     */
    public function getSolarSystem()
    {
        return $this->solarSystem;
    }

    /**
     * @param array $systemCostIndices
     */
    public function setSystemCostIndices($systemCostIndices)
    {
        $this->systemCostIndices = $systemCostIndices;
    }

    /**
     * @return array
     */
    public function getSystemCostIndices()
    {
        return $this->systemCostIndices;
    }
}

