<?php
namespace GbsLogistics\Doramad\Domain;

class IndustrySpeciality implements CrestResourceInterface
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $specializationId;
    /** @var array<int> */
    protected $groups;
    /** @var string */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $groups
     * @return $this
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $specializationId
     * @return $this
     */
    public function setSpecializationId($specializationId)
    {
        $this->specializationId = $specializationId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpecializationId()
    {
        return $this->specializationId;
    }


}
