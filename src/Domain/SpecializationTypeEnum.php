<?php
namespace GbsLogistics\Doramad\Domain;


final class SpecializationTypeEnum
{
    const INVALID = -1;
    const TE = 0;
    const ME = 1;

    private function __construct() {}
}

