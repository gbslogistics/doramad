<?php
namespace GbsLogistics\Doramad\Domain;

class IndustryTeam implements CrestResourceInterface
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $teamId;
    /** @var SolarSystem */
    protected $solarSystem;
    /** @var integer */
    protected $specializationId;
    /** @var \DateTime */
    protected $expiryTime;
    /** @var array<Worker> */
    protected $workers = [];
    /** @var \DateTime */
    protected $creationTime;
    /** @var float */
    protected $costModifier;
    /** @var \DateTime */
    protected $auctionExpiryTime;
    /** @var array<SolarSystemBid> */
    protected $solarSystemBids = [];
    /** @var int */
    protected $activityId;
    /** @var string */
    protected $name;

    function __construct()
    {
        $this->solarSystem = new SolarSystem();
        $this->expiryTime = new \DateTime();
        $this->creationTime = new \DateTime();
        $this->auctionExpiryTime = new \DateTime();
    }

    /**
     * @param float $costModifier
     * @return $this
     */
    public function setCostModifier($costModifier)
    {
        $this->costModifier = $costModifier;
        return $this;
    }

    /**
     * @return float
     */
    public function getCostModifier()
    {
        return $this->costModifier;
    }

    /**
     * @param \DateTime $creationTime
     * @return $this
     */
    public function setCreationTime($creationTime)
    {
        $this->creationTime = $creationTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationTime()
    {
        return $this->creationTime;
    }

    /**
     * @param \DateTime $expiryTime
     * @return $this
     */
    public function setExpiryTime($expiryTime)
    {
        $this->expiryTime = $expiryTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiryTime()
    {
        return $this->expiryTime;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \GbsLogistics\Doramad\Domain\SolarSystem $solarSystem
     * @return $this
     */
    public function setSolarSystem($solarSystem)
    {
        $this->solarSystem = $solarSystem;
        return $this;
    }

    /**
     * @return \GbsLogistics\Doramad\Domain\SolarSystem
     */
    public function getSolarSystem()
    {
        return $this->solarSystem;
    }

    /**
     * @param integer $specializationId
     * @return $this
     */
    public function setSpecializationId($specializationId)
    {
        $this->specializationId = $specializationId;
        return $this;
    }

    /**
     * @return integer
     */
    public function getSpecializationId()
    {
        return $this->specializationId;
    }

    /**
     * @param int $teamId
     * @return $this
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @param array $workers
     * @return $this
     */
    public function setWorkers($workers)
    {
        $this->workers = $workers;
        return $this;
    }

    /**
     * @return array
     */
    public function getWorkers()
    {
        return $this->workers;
    }

    /**
     * @param \DateTime $auctionExpiryTime
     * @return $this
     */
    public function setAuctionExpiryTime($auctionExpiryTime)
    {
        $this->auctionExpiryTime = $auctionExpiryTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAuctionExpiryTime()
    {
        return $this->auctionExpiryTime;
    }

    /**
     * @param array $solarSystemBids
     * @return $this
     */
    public function setSolarSystemBids($solarSystemBids)
    {
        $this->solarSystemBids = $solarSystemBids;
        return $this;
    }

    /**
     * @return array
     */
    public function getSolarSystemBids()
    {
        return $this->solarSystemBids;
    }

    /**
     * @param int $activityId
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;
    }

    /**
     * @return int
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
