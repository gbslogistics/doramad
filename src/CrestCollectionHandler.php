<?php

namespace GbsLogistics\Doramad;


use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;

class CrestCollectionHandler implements SubscribingHandlerInterface
{

    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'crest_collection',
                'method' => 'deserializeCrestCollection'
            ]
        ];
    }

    public function deserializeCrestCollection(
        JsonDeserializationVisitor $visitor,
        array $data,
        /** @noinspection PhpUnusedParameterInspection */
        array $type,
        Context $context
    ) {
        $collectionClass = $context->attributes
            ->get(CrestDeserializationContext::COLLECTION_CLASS_KEY)
            ->getOrThrow(new \RuntimeException('Context did not contain a collection class when attempting to deserialize an attribute of type crest_collection. Please use the CrestDeserializationContext and add the class that is expected via the setCollectionClass method.'));

        /** @noinspection PhpVoidFunctionResultUsedInspection */
        return $visitor->visitArray(
            $data,
            [
                'name' => 'array',
                'params' => [ [
                    'name' => $collectionClass
                ] ],
            ],
            $context
        );
    }
}