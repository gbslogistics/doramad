<?php

namespace GbsLogistics\Doramad;


use JMS\Serializer\DeserializationContext;

class CrestDeserializationContext extends DeserializationContext
{
    const COLLECTION_CLASS_KEY = 'doramad_collection_class';

    /**
     * @return string
     */
    public function getCollectionClass()
    {
        return $this->attributes->get(self::COLLECTION_CLASS_KEY);
    }

    /**
     * @param string $collectionClass
     */
    public function setCollectionClass($collectionClass)
    {
        $this->setAttribute(self::COLLECTION_CLASS_KEY, $collectionClass);
    }
}
