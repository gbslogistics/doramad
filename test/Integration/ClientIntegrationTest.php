<?php


namespace GbsLogistics\Doramad\Test\Integration;
use GbsLogistics\Doramad\Client;
use GbsLogistics\Doramad\Domain\IndustryFacility;
use GbsLogistics\Doramad\Domain\IndustrySpeciality;
use GbsLogistics\Doramad\Domain\IndustrySystem;
use GbsLogistics\Doramad\Domain\IndustryTeam;


/**
 * Class ClientIntegrationTest
 * @package GbsLogistics\Doramad\Test\Integration
 * @group integration
 */
class ClientIntegrationTest extends \PHPUnit_Framework_TestCase
{
    /** @var Client */
    private $sut;

    public function setUp()
    {
        $this->sut = new Client();
    }

    public function testGetIndustryTeams()
    {
        $teams = $this->sut->loadCrestResource('http://public-crest.eveonline.com/industry/teams/auction/');
        $this->assertInternalType('array', $teams);
        $this->assertNotCount(0, $teams);
        $this->assertContainsOnlyInstancesOf(IndustryTeam::class, $teams);

        /** @var IndustryTeam $team */
        foreach ($teams as $team) {
            $this->assertNotNull($team->getTeamId());
        }

        return $teams[0];
    }

    /** @depends testGetIndustryTeams */
    public function testGetIndividualTeam(IndustryTeam $returnedTeam)
    {
        $teamId = $returnedTeam->getTeamId();

        $team = $this->sut->loadCrestResource('http://public-crest.eveonline.com/industry/teams/' . $teamId . '/');
        $this->assertInstanceOf(IndustryTeam::class, $team);
    }

    public function testGetSpecialties()
    {
        $specialties = $this->sut->loadCrestResource('http://public-crest.eveonline.com/industry/specialities/');
        $this->assertInternalType('array', $specialties);
        $this->assertNotCount(0, $specialties);
        $this->assertContainsOnlyInstancesOf(IndustrySpeciality::class, $specialties);

        /** @var IndustrySpeciality $specialty */
        foreach ($specialties as $specialty) {
            $this->assertNotNull($specialty->getSpecializationId());
        }
    }

    public function testGetIndustrySystems()
    {
        $industrySystems = $this->sut->loadCrestResource('http://public-crest.eveonline.com/industry/systems/');
        $this->assertInternalType('array', $industrySystems);
        $this->assertNotCount(0, $industrySystems);
        $this->assertContainsOnlyInstancesOf(IndustrySystem::class, $industrySystems);
    }

    public function testGetIndustryFacilities()
    {
        $industryFacilities = $this->sut->loadCrestResource('http://public-crest.eveonline.com/industry/facilities/');
        $this->assertInternalType('array', $industryFacilities);
        $this->assertNotCount(0, $industryFacilities);
        $this->assertContainsOnlyInstancesOf(IndustryFacility::class, $industryFacilities);

        /** @var IndustryFacility $facility */
        foreach ($industryFacilities as $facility) {
            $this->assertNotNull($facility->getTypeId());
        }
    }
}


