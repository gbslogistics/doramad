<?php

namespace GbsLogistics\Doramad\Test;


use GbsLogistics\Doramad\Client;
use GbsLogistics\Doramad\CrestException;
use GbsLogistics\Doramad\Domain\CrestResourceInterface;
use GbsLogistics\Doramad\Domain\IndustryTeam;
use GuzzleHttp\Adapter\StreamAdapter;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Subscriber\Mock;
use JMS\Serializer\SerializerInterface;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    const TEST_URL = 'http://example.com';

    /** @var Client */
    private $sut;

    /**
     * @var SerializerInterface
     * @Mock
     */
    private $serializer;

    public function setUp()
    {
        $this->sut = new Client([], $this->serializer);
    }

    public function testLoadCrestResourceThrowsWhenContentTypeFailsRegex()
    {
        $contentType = $this->getContentTypeHeader('nonsense-header');
        $this->attachGuzzleMock($contentType);
        $this->setExpectedException(CrestException::class, sprintf(
            'Return type for url "%s" was not a valid CREST content type; got "%s" instead.',
            self::TEST_URL,
            $contentType['Content-Type']
        ));
        $this->sut->loadCrestResource(self::TEST_URL);
    }

    public function testLoadCrestResourceThrowsWhenContentTypeIsNonexistentClass()
    {
        $class = 'shazbot';
        $contentType = $this->getValidContentTypeHeader($class);
        $this->attachGuzzleMock($contentType);
        $this->setExpectedException(CrestException::class, sprintf(
            'Class "%s" does not exist; derived from content type "%s".',
            Client::TARGET_NAMESPACE . $class,
            $contentType['Content-Type']
        ));
        $this->sut->loadCrestResource(self::TEST_URL);
    }

    public function testLoadCrestResourceThrowsWhenContentTypeIsClassNotImplementingCrestResourceInterface()
    {
        $solarSystemClass = 'SolarSystem';
        $contentType = $this->getValidContentTypeHeader($solarSystemClass);

        $this->attachGuzzleMock($contentType);
        $this->setExpectedException(CrestException::class, sprintf(
            'Class "%s" derived from content type "%s" is not a Crest Resource.',
            Client::TARGET_NAMESPACE . $solarSystemClass,
            $contentType['Content-Type']
        ));
        $this->sut->loadCrestResource(self::TEST_URL);
    }

    public function testLoadCrestResourceWithSingleton()
    {
        $contentType = $this->getValidContentTypeHeader('IndustryTeam');
        $body = $this->getGuzzleStreamInterfaceForFixture(__DIR__ . '/fixtures/single_team.json');
        $this->attachGuzzleMock($contentType, $body);

        $output = $this->sut->loadCrestResource(self::TEST_URL);
        $this->assertInstanceOf(IndustryTeam::class, $output);
    }

    public function testLoadCrestResourceWithMultipleObjects()
    {
        $contentType = $this->getValidContentTypeHeader('IndustryTeamCollection');
        $body = $this->getGuzzleStreamInterfaceForFixture(__DIR__ . '/fixtures/teams.json');
        $this->attachGuzzleMock($contentType, $body);

        $output = $this->sut->loadCrestResource(self::TEST_URL);
        $this->assertContainsOnlyInstancesOf(IndustryTeam::class, $output);
        $body->seek(0);
        $parsedJson = json_decode($body->getContents(), true);
        $this->assertCount($parsedJson['totalCount'], $output);
    }

    private function attachGuzzleMock($headers, $content = null)
    {
        $mock = new Mock([
            new Response(200, $headers, $content)
        ]);

        $this->sut->getEmitter()->attach($mock);
    }

    private function getContentTypeHeader($contentType)
    {
        return [ 'Content-Type' => $contentType ];
    }

    private function getValidContentTypeHeader($class)
    {
        return $this->getContentTypeHeader(sprintf(
            'application/vnd.ccp.eve.%s-v1+json; charset=utf-8',
            $class
        ));
    }

    private function getGuzzleStreamInterfaceForFixture($content)
    {
        return Stream::factory(file_get_contents($content));
    }
}
