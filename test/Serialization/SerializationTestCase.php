<?php

namespace GbsLogistics\Doramad\Test\Serialization;


use GbsLogistics\Doramad\SerializerFactory;
use JMS\Serializer\Serializer;

class SerializationTestCase extends \PHPUnit_Framework_TestCase
{
    /** @var Serializer */
    protected $serializer;

    public function setUp()
    {
        $this->serializer = SerializerFactory::getSerializer();
    }

    protected function getFixtureFilePath($filename)
    {
        return realpath(sprintf(__DIR__ . '/../fixtures/%s', $filename));
    }

    protected function getDate($dateString)
    {
        return new \DateTime($dateString, new \DateTimeZone('UTC'));
    }
}
 