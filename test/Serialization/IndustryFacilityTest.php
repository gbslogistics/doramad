<?php

namespace GbsLogistics\Doramad\Test\Serialization;


use GbsLogistics\Doramad\Domain\IndustryFacility;

class IndustryFacilityTest extends SerializationTestCase
{
    public function testDeserializeIndustryFacility()
    {
        $fixtureFile = $this->getFixtureFilePath('single_industry_facility.json');
        /** @var IndustryFacility $facility */
        $facility = $this->serializer->deserialize(
            file_get_contents($fixtureFile),
            IndustryFacility::class,
            'json'
        );

        $this->assertInstanceOf(IndustryFacility::class, $facility);
        $this->assertEquals(60012544, $facility->getFacilityId());
        $this->assertEquals(30000032, $facility->getSolarSystemId());
        /** @noinspection SpellCheckingInspection */
        $this->assertEquals('Hasiari VIII - Moon 4 - Ammatar Consulate Bureau', $facility->getFacilityName());
        $this->assertEquals(10000001, $facility->getRegionId());
        $this->assertEquals(0.1, $facility->getTax());
        $this->assertEquals(1000126, $facility->getOwnerId());
        $this->assertEquals(2502, $facility->getTypeId());
    }
}
 