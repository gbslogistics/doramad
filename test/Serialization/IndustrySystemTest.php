<?php

namespace GbsLogistics\Doramad\Test\Serialization;
use GbsLogistics\Doramad\Domain\IndustrySystem;
use GbsLogistics\Doramad\Domain\SolarSystem;
use GbsLogistics\Doramad\Domain\SystemCostIndex;


/**
 *
 *
 * @author Querns <querns@gbs.io>
 */
class IndustrySystemTest extends SerializationTestCase
{
    public function testDeserializeIndustrySystem()
    {
        $fixtureFile = $this->getFixtureFilePath('single_industry_system.json');
        /** @var IndustrySystem $industrySystem */
        $industrySystem = $this->serializer->deserialize(
            file_get_contents($fixtureFile),
            IndustrySystem::class,
            'json'
        );

        $this->assertInstanceOf(IndustrySystem::class, $industrySystem);
        $solarSystem = $industrySystem->getSolarSystem();
        $systemCostIndices = $industrySystem->getSystemCostIndices();

        $this->assertCount(6, $systemCostIndices);
        $this->assertContainsOnlyInstancesOf(SystemCostIndex::class, $systemCostIndices);
        $this->assertInstanceOf(SolarSystem::class, $solarSystem);


        /** @noinspection SpellCheckingInspection */
        $this->assertEquals("Jouvulen", $solarSystem->getName());
        /** @var SystemCostIndex $index */
        foreach ($systemCostIndices as $index) {
            $this->assertNotNull($index->getActivityId());
            $this->assertNotNull($index->getActivityName());
            $this->assertNotNull($index->getCostIndex());
        }
    }
}
