<?php

namespace GbsLogistics\Doramad\Test\Serialization;


use GbsLogistics\Doramad\Domain\IndustrySpeciality;

class IndustrySpecialtyTest extends SerializationTestCase
{
    public function testDeserializeSpecialization()
    {
        $fixtureFile = $this->getFixtureFilePath('single_specialty.json');
        /** @var IndustrySpeciality $specialization */
        $specialization = $this->serializer->deserialize(
            file_get_contents($fixtureFile),
            IndustrySpeciality::class,
            'json'
        );

        $this->assertInstanceOf(IndustrySpeciality::class, $specialization);
        $this->assertEquals(7, $specialization->getSpecializationId());
        /** @noinspection SpellCheckingInspection */
        $this->assertEquals('Starbase', $specialization->getName());
        $this->assertContainsOnly('integer', $specialization->getGroups());
        $this->assertCount(24, $specialization->getGroups());
    }
}
