<?php

namespace GbsLogistics\Doramad\Test\Serialization;


use GbsLogistics\Doramad\Domain\Character;
use GbsLogistics\Doramad\Domain\SolarSystem;
use GbsLogistics\Doramad\Domain\SpecializationTypeEnum;
use GbsLogistics\Doramad\Domain\IndustryTeam\CharacterBid;
use GbsLogistics\Doramad\Domain\IndustryTeam\SolarSystemBid;
use GbsLogistics\Doramad\Domain\IndustryTeam\Worker;
use GbsLogistics\Doramad\Domain\IndustryTeam;

class TeamTest extends SerializationTestCase
{
    public function testDeserializingTeamInAuction()
    {
        $fixtureFile = $this->getFixtureFilePath('single_team_auction.json');

        /** @var IndustryTeam $industryTeam */
        $industryTeam = $this->serializer->deserialize(
            file_get_contents($fixtureFile),
            IndustryTeam::class,
            'json'
        );

        $this->assertInstanceOf(IndustryTeam::class, $industryTeam);

        $solarSystem = $industryTeam->getSolarSystem();
        $this->assertEquals(30022547, $solarSystem->getSolarSystemId());
        /** @noinspection SpellCheckingInspection */
        $this->assertEquals('Krilmokenur', $solarSystem->getName());
        $this->assertEquals(3, $industryTeam->getSpecializationId());
        $this->assertEquals($this->getDate('2014-08-23T17:33:19'), $industryTeam->getExpiryTime());
        $this->assertEquals($this->getDate('2014-07-19T17:33:19'), $industryTeam->getCreationTime());
        $this->assertEquals($this->getDate('2014-07-26T17:33:19'), $industryTeam->getAuctionExpiryTime());
        $this->assertEquals(8.0, $industryTeam->getCostModifier());
        $this->assertEquals('Core Complexion Inc.<br>Team MCK00', $industryTeam->getName());
        $this->assertEquals(1, $industryTeam->getActivityId());

        $workers = $industryTeam->getWorkers();
        $this->assertCount(4, $workers);
        $this->assertContainsOnlyInstancesOf(Worker::class, $workers);

        /** @var Worker $worker */
        foreach ($workers as $worker) {
            $workerBonus = $worker->getBonus();
            $this->assertNotNull($workerBonus);
            $this->assertContains($workerBonus->getBonusType(), [ SpecializationTypeEnum::ME, SpecializationTypeEnum::TE ]);
            $this->assertTrue(is_float($workerBonus->getValue()), 'Expected worker bonus value to be a float.');
        }

        $solarSystemBids = $industryTeam->getSolarSystemBids();
        $this->assertContainsOnlyInstancesOf(SolarSystemBid::class, $solarSystemBids);
        $this->assertCount(10, $solarSystemBids);

        /** @var SolarSystemBid $bid */
        foreach ($solarSystemBids as $bid) {
            $this->assertTrue(is_float($bid->getBidAmount()), 'Expected solar system bid amount to be a float.');

            $characterBids = $bid->getCharacterBids();
            $this->assertContainsOnlyInstancesOf(CharacterBid::class, $characterBids);

            /** @var CharacterBid $characterBid */
            foreach ($characterBids as $characterBid) {
                $this->assertTrue(is_float($characterBid->getBidAmount()), 'Expected character bid amount to be a float.');

                $character = $characterBid->getCharacter();
                $this->assertInstanceOf(Character::class, $character);
                $this->assertNotNull($character->getCharacterId());
                $this->assertNotNull($character->getName());
                $this->assertNotNull($character->getIsNpc());
            }

            $bidSolarSystem = $bid->getSolarSystem();
            $this->assertInstanceOf(SolarSystem::class, $bidSolarSystem);
            $this->assertNotNull($bidSolarSystem->getSolarSystemId());
            $this->assertNotNull($bidSolarSystem->getName());
        }
    }

    public function testDeserializeCharteredTeam()
    {
        $fixtureFile = $this->getFixtureFilePath('single_team.json');
        /** @var IndustryTeam $industryTeam */
        $industryTeam = $this->serializer->deserialize(
            file_get_contents($fixtureFile),
            IndustryTeam::class,
            'json'
        );

        $this->assertInstanceOf(IndustryTeam::class, $industryTeam);

        $solarSystem = $industryTeam->getSolarSystem();
        $this->assertEquals("30001107", $solarSystem->getSolarSystemId());
        $this->assertEquals('QZ1-OH', $solarSystem->getName());
        $this->assertEquals(4, $industryTeam->getSpecializationId());
        $this->assertEquals($this->getDate('2014-08-19T14:23:17'), $industryTeam->getExpiryTime());
        $this->assertEquals($this->getDate('2014-07-15T14:23:17'), $industryTeam->getCreationTime());
        $this->assertEquals(4.0, $industryTeam->getCostModifier());
        $this->assertEquals('Caldari Steel<br>Team MSA00', $industryTeam->getName());
        $this->assertEquals(1, $industryTeam->getActivityId());

        $workers = $industryTeam->getWorkers();
        $this->assertCount(4, $workers);
        $this->assertContainsOnlyInstancesOf(Worker::class, $workers);

        /** @var Worker $worker */
        foreach ($workers as $worker) {
            $workerBonus = $worker->getBonus();
            $this->assertNotNull($workerBonus);
            $this->assertContains($workerBonus->getBonusType(), [ SpecializationTypeEnum::ME, SpecializationTypeEnum::TE ]);
            $this->assertTrue(is_float($workerBonus->getValue()), 'Expected worker bonus value to be a float.');
        }
    }
}
